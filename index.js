// buttons

const buttonGetLoan = document.getElementById("button-getLoan")
const buttonBank = document.getElementById("button-bank")
const buttonWork = document.getElementById("button-work")
const buttonRepay = document.getElementById("button-repay")
const buttonBuy = document.getElementById("button-buy")



//  Elements

const bankTotal = document.getElementById("bank-total")
const loanTotal = document.getElementById("loan-total")
const workTotal = document.getElementById("work-total")
const laptopTotal = document.getElementById("laptop-total")
const loanSection = document.getElementById("loan-section")
const myName = document.getElementById("my-name")


const specsElement = document.getElementById("specs-list")
const imgElement = document.getElementById("laptop-image")
const titleElement = document.getElementById("laptop-title")
const descriptionElement = document.getElementById("laptop-description")
const priceElement = document.getElementById("laptop-price")

const optionsElement = document.getElementById("laptop-select")

// Events

buttonBank.addEventListener("click", transferToBank)
buttonWork.addEventListener("click", addStepToWorkTotal)
buttonGetLoan.addEventListener("click", getLoan)
buttonRepay.addEventListener("click", repayLoan)
buttonBuy.addEventListener("click", buyNow)

buttonRepay.style.display = "none"


//  api related 

let laptops = []

const API_URL = "https://hickory-quilled-actress.glitch.me/computers"

fetch(API_URL)
    .then(response => response.json())
    .then(json => laptops = json)
    .then(laptops => { addLaptopsToOptions(laptops), showDetails(0), changeDetails() });


const addLaptopsToOptions = (laptops) => {
    console.log(laptops)
    laptops.forEach(x => addLaptopToOptions(x));
}

const addLaptopToOptions = (laptop) => {
    const optionElement = document.createElement("option")
    optionElement.value = laptop.id;
    optionElement.appendChild(document.createTextNode(laptop.title))
    optionsElement.appendChild(optionElement)
}

function showDetails(id) {
    specsElement.innerHTML = ""     // first we clear the list
    // imgElement.removeChild(imgElement.firstChild)
    imgElement.innerHTML = "" 

    let data = laptops[id].specs
    data.forEach((item) => {
        let li = document.createElement("li");      //rendering all specs of a latpop in a html ul
        li.innerText = item;
        specsElement.appendChild(li);
    })

    // rendering all the rest info of the laptop
    let img = document.createElement("img");
    img.src='https://hickory-quilled-actress.glitch.me/' + laptops[id].image
    img.alt=laptops[id].title
    imgElement.appendChild(img) 
    titleElement.innerHTML = laptops[id].title
    descriptionElement.innerHTML = laptops[id].description
    priceElement.innerHTML = laptops[id].price
    checkValue()
}

function changeDetails() {
    optionsElement.addEventListener("change", function () { showDetails(this.value - 1) })
    checkValue()
}

// functions for buttons

function getLoan() {
    if (0.00 !== getLoanTotal()) return;
    var loan = window.prompt("Tell us the amount of the loan you need.\n" + "It must be no more than " + 2 * getBankTotal() + ".")
    if (loan == null) return;
    if (isNaN(parseFloat(loan)) || loan > 2 * parseFloat(getBankTotal())) {
        return getLoan()
    }
    updateLoanTotal(loan)
    updateBankTotal(getBankTotal() + getLoanTotal())
    checkValue()
}

function transferToBank() {
    let step2 = getWorkTotal()
    if (getLoanTotal() !== 0.00) {
        if (0.1 * parseFloat(step2) < getLoanTotal()) {
            updateLoanTotal(getLoanTotal() - 0.1 * parseFloat(step2))
            updateBankTotal(getBankTotal() + 0.9 * parseFloat(step2))
        } else {
            updateBankTotal(getBankTotal() + parseFloat(step2) - getLoanTotal())
            updateLoanTotal(0.00)
        }
    } else {
        addToBankTotal(step2);
    }
    updateWorkTotal(0);
    checkValue()
}

const step = 100;
function addStepToWorkTotal() {
    addToWorkTotal(step);
}

function repayLoan() {
    if ((getWorkTotal() + getBankTotal()) >= getLoanTotal()) {
        updateBankTotal(getWorkTotal() + getBankTotal() - getLoanTotal())
        updateLoanTotal(0)
    } else {
        updateLoanTotal(getLoanTotal() - getWorkTotal())
    }
    updateWorkTotal(0)
    checkValue()
}

function buyNow() {
    if (getPrice() <= getBankTotal()) {
        updateBankTotal(getBankTotal() - getPrice())
        alert("You are now the owner of the laptop " + "'" + titleElement.innerHTML + "'.")
    } else alert("You cannot afford this laptop")
    checkValue()
}

// Checking functions

function checkValue() {
    if (getPrice() <= getBankTotal()) buttonBuy.style.background = "green"
    if (getPrice() > getBankTotal()) buttonBuy.style.background = "#9f0202"
    if (getLoanTotal() === 0.00) { hide(), buttonGetLoan.style.background = "#64e696" }
    if (getLoanTotal() !== 0.00) { unHide(), buttonGetLoan.style.background = "#9f0202" }
}

function unHide() {
    buttonRepay.style.display = ""
}

function hide() {
    buttonRepay.style.display = "none"
}

// functions for refactoring


function addToWorkTotal(value) {
    updateWorkTotal(getWorkTotal() + parseFloat(value));
}

function updateWorkTotal(value) {
    workTotal.innerText = parseFloat(value).toFixed(2);
}

function getWorkTotal() {
    return parseFloat(workTotal.innerText);
}

function addToBankTotal(value) {
    updateBankTotal(getBankTotal() + parseFloat(value));
}

function updateBankTotal(value) {
    bankTotal.innerText = parseFloat(value).toFixed(2);
}

function getBankTotal() {
    return parseFloat(bankTotal.innerText);
}

function addToLoanTotal(value) {
    updateLoanTotal(getLoanTotal() + parseFloat(value));
}

function updateLoanTotal(value) {
    loanTotal.innerText = parseFloat(value).toFixed(2);
}

function getLoanTotal() {
    return parseFloat(loanTotal.innerText);
}

function updatePrice(value) {
    priceElement.innerHTML = parseFloat(value).toFixed(2);
}

function getPrice() {
    return parseFloat(priceElement.innerText)
}