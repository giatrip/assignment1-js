# assignment1-js



## Description

A  computer site that supports the main actions a user can do.

These actions are :

1. Get a loan from the store (only applicable if you ask for the correct amount ).
2. Add money to your account.
3. Work and increase your balance .
4. Repay your loan (if you have one!)
5. See the items store provides and theiR detailes.
6. Buy whatever you need (if you can afford it).


## Contributors

Giannis Tripodis
